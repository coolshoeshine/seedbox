from django.shortcuts import render
from django.views import View

# Create your views here.
class DownloadView(View):
    template_name = "download.html"

    def get_context(self, request):
        context = {}
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)