from django.db import models
from django.contrib.auth.models import User
from apps.content.models import Contract
import json

# Create your models here.
class Seeder(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255) # user specified name for this seeder

    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # seeder parameters
    disk_space = models.IntegerField(default=0) # amount of disk space in bytes
    upload_bandwidth = models.IntegerField(default=0) # bandwidth in bytes/s

    # payments
    cost_per_byte = models.FloatField(default=0) # sats/byte fee (actually per mb)

    # history
    torrent_id = models.IntegerField(default=0)
    bytes_seeded = models.IntegerField(default=0) # bytes seeded so far
    bytes_delta = models.IntegerField(default=0)
    mb_delta = models.FloatField(default=0) # diff in MB seeded thus far
    stars = models.FloatField(default=0) # seeder rating (0-5)

    #Contract
    contract = models.ForeignKey(Contract, null=True, on_delete=models.CASCADE) #CASCADE ensures if ever the contract is deleted so would this object

    def __str__(self):
        s = "%s | %.3f GB disk | %.3f MB/s upload | %f sats/kb | torrent_id: %d" % (
            self.name,
            self.disk_space / 2**30,
            self.upload_bandwidth / 2**20,
            self.cost_per_byte / 2**10,
            self.torrent_id,
        )
        return s




class SeedBoxParameters(models.Model):
    pubkey = models.CharField(max_length=255, primary_key=True) # LND pubkey

    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # user-set parameters
    status = models.BooleanField(default=False)
    name = models.CharField(max_length=255, null=True) # nickname
    max_disk_space = models.FloatField(default=0) # measured in GB
    max_torrent_size = models.FloatField(default=0) # measured in GB
    fee = models.IntegerField(default=0) # sats per MB
    maxUploadRate = models.FloatField(default=0) # max KBPS upload rate
    maxDailyUpload = models.FloatField(default=0) # max GB uploaded per day

    # machine constraint parameters
    total_available_disk_space = models.FloatField(default=0) # measured in GB


    def __str__(self):
        s = "%s enabled: %s | %s GB for %s sats/MB" % (
            self.name,
            self.status,
            self.max_disk_space,
            self.fee,
        )
        return s

    
    def reset(self):
        self.status = False
        self.name = None
        self.max_disk_space = None
        self.max_torrent_size = None
        self.fee = None
        self.maxUploadRate = None
        self.maxDailyUpload = None
        self.save()


    """
        function: offer_json
        Returns a JSON formatted string with the offer details
        Meant to be passed directly as a message through Impervious
    """
    def offer_json(self):
        offer = {
            "message_type": "seedbox_offer",
            "disk_space": self.max_disk_space,
            "upload_bandwidth": self.maxUploadRate,
            "cost_per_mb": self.fee,
        }
        return json.dumps(offer)
