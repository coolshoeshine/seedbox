from django import forms
from .models import Seeder

class NewSeedboxForm(forms.ModelForm):
    class Meta:
        model = Seeder
        fields = ['name', 'disk_space', 'upload_bandwidth', 'cost_per_byte']
