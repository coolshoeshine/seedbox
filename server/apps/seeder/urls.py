from django.urls import path
from . import views

urlpatterns = [
    # path('seed', views.SeederView.as_view(), name="seed"),
    path('', views.SeedboxView.as_view(), name="seedbox"),
]
