from .models import Seeder
from django.shortcuts import render
from django.views import View
from .forms import NewSeedboxForm
from .models import SeedBoxParameters
import transmissionrpc
from src.tsm_rpc.get_torrents import get_torrents


# Create your views here.

class SeedboxView(View):
    template_name = "seedbox.html"

    def get_context(self, request):
        context = {}

        # get torrents (note, a bit more work is required than just the query, for things like the status bar)
        torrents = get_torrents()
        context['torrents'] = torrents

        # get user's seedbox settings
        pubkey = "local" # there should be only 1 instance of this object in the local database
        seedbox_settings, was_created = SeedBoxParameters.objects.using('localuser').get_or_create(pubkey=pubkey)
        context['seedbox_settings'] = seedbox_settings
        print("seedbox_settings: ", seedbox_settings)

        # get remaining capacity status
        remaining_storage_capacity = 75 # %
        remaining_daily_upload = 80 # %
        context['remaining_storage_capacity'] = remaining_storage_capacity
        context['remaining_daily_upload'] = remaining_daily_upload

        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)

        # Torrents tab
        if 'remove_torrent_id' in request.POST:
            id = int(request.POST['remove_torrent_id'])
            print("Removing torrent ID: " % id)
            src.tsm_rpc.remove_torrent(id)

        elif 'pause_torrent_id' in request.POST:
            id = int(request.POST['pause_torrent_id'])
            print("this is where we run pause_torrent.py for torrent id %d" % id)

        # Settings tab
        elif 'cancel' in request.POST:
            pass # do nothing

        elif 'reset' in request.POST:
            seedbox_settings = context['seedbox_settings']
            seedbox_settings.reset()
            context['seedbox_settings'] = seedbox_settings

        elif 'submit' in request.POST:
            # extract parameters from request.POST
            status = request.POST['status']
            seedboxName = request.POST['seedboxName']
            max_disk_space = request.POST['max_disk_space']
            # max_torrent_size = request.POST['max_torrent_size']
            fee = request.POST['fee']
            # maxUploadRate = request.POST['maxUploadRate']
            # maxDailyUpload = request.POST['maxDailyUpload']

            # update the seedbox settings
            seedbox_settings = context['seedbox_settings']
            if status == "Seedboxing On":
                seedbox_settings.status = True
            else:
                seedbox_settings.status = False
            seedbox_settings.name = seedboxName
            seedbox_settings.max_disk_space = max_disk_space
            # seedbox_settings.max_torrent_size = max_torrent_size
            seedbox_settings.fee = fee
            # seedbox_settings.maxUploadRate = maxUploadRate
            # seedbox_settings.maxDailyUpload = maxDailyUpload
            seedbox_settings.save()
            context['seedbox_settings'] = seedbox_settings

        return render(request, self.template_name, context)







class SeederView(View):
    template_name = "dashboard/seed.html"

    def get_context(self, request):
        context = {}

        # prep form
        context['new_seedbox_form'] = NewSeedboxForm(request.POST or None)

        # find existing seeds for this user
        existing_seedboxes = Seeder.objects.filter(user=request.user)
        context['existing_seedboxes'] = existing_seedboxes

        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)

        # parse the form
        new_seedbox_form = context['new_seedbox_form']
        if new_seedbox_form.is_valid():
            seeder = new_seedbox_form.save(commit=False)
            seeder.user = request.user
            seeder.save()

        return render(request, self.template_name, context)

