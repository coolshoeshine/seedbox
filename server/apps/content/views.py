from django.shortcuts import render
from django.views import View
from apps.accounts.models import KnownOffers
from .forms import UploadFileForm
from .models import UserContent, Contract, ContractStatus, ContractInvoices
import time



class ContentView(View):
    template_name = "content.html"

    def get_context(self, request):
        context = {}

        # display all user content
        all_user_content = UserContent.objects.using('localuser').all()
        context['contents'] = all_user_content

        # get all contracts
        all_contracts = Contract.objects.using('localuser').all()
        context['contracts'] = all_contracts

        # populate address book
        addressBook = KnownOffers.objects.using('localuser').all()
        context['addressBook'] = addressBook

        return context

    def get(self, request):
        context = self.get_context(request)
        context['upload_form'] = UploadFileForm()
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)

        # did user press "add_pubkey"
        if "add_pubkey" in request.POST:
            pubkey = request.POST['pubkey']
            print('pubkey: ', pubkey)

            # get/create known offer and query
            offer, was_created = KnownOffers.objects.using('localuser').get_or_create(pubkey=pubkey)
            offer.query()

        # did user press 'upload_file'
        elif 'upload_file' in request.POST:
            form = UploadFileForm(request.POST, request.FILES)
            if form.is_valid():
                # create the object in the database (this will automatically upload the file)
                user_content = UserContent.objects.using('localuser').create(upload=request.FILES['upload'])
                user_content.name = str(request.FILES['upload'])
                user_content.save()
                user_content.set_size()

        # upload new content tab
        elif 'cancel' in request.POST:
            pass # do nothing

        elif 'search_address_book' in request.POST:
            # query all known offers again
            all_offers = KnownOffers.objects.using('localuser').all()
            for offer in all_offers:
                offer.query()

            # short delay (ghetto-rigged temp solution)
            # offer query responses are handled in server/apps/websocket/views.py websocket_test_datastream()
            time.sleep(2) # 2 seconds

            # get file from database
            chosen_file = request.POST['chosen_file']
            file_db_obj = UserContent.objects.using('localuser').get(name=chosen_file)
            request.session['chosen_file'] = chosen_file
            
            # return all offers with enough disk space
            # KnownOffers.disk_space is measured in GB
            matching_offers = KnownOffers.objects.using('localuser').filter(
                disk_space__gte=file_db_obj.size_mb / 2**10,
            )

            # write to context dictionary
            context['matching_offers'] = matching_offers

        # did user accept an offer?
        elif 'accept_offer_id' in request.POST:
            offer_pubkey = request.POST['accept_offer_id']
            # offer = KnownOffers.objects.using('localuser').get(pubkey=offer_pubkey)
            # print(offer)

            # fetch the contract object
            print("offer_pubkey: ", offer_pubkey)
            contract = Contract.objects.using('localuser').filter(
                seedbox_pubkey=offer_pubkey,
            ).order_by("-time_created").first()

            # get the file object and update the contract with file info
            chosen_file = request.session['chosen_file']
            file_db_obj = UserContent.objects.using('localuser').get(name=chosen_file)
            contract.path_to_file = file_db_obj.full_path
            contract.content_name = file_db_obj.name
            contract.file_size = file_db_obj.size_mb
            contract.save()

            # accept the offer
            contract.AcceptOffer()
            # file transfer happens in the websocket method

        # did user press terminate contract?
        elif 'cancel_contract_id' in request.POST:
            contract_id = request.POST['cancel_contract_id']
            contract = Contract.objects.using('localuser').get(contract_id=contract_id)
            contract.status = ContractStatus.EXPIRED
            contract.save()
            
        # did user press delete address?
        elif 'remove_address_id' in request.POST:
            pubkey = request.POST['remove_address_id']
            KnownOffers.objects.using('localuser').get(pubkey=pubkey).delete()
                
        
        # reset the form
        context['upload_form'] = UploadFileForm()

        return render(request, self.template_name, context)




class InvoicesView(View):
    template_name = "invoices.html"

    def get_context(self, request):
        context = {}
        
        # get all invoices
        invoice_objs = ContractInvoices.objects.using('localuser').all().order_by("-time_created")
        # format for content table
        invoices = []
        for invoice in invoice_objs:
            time_str = invoice.time_created.strftime("%d %b %y | %I:%M:%S%p")
            amount = invoice.amount
            contract = invoice.contract
            if contract.purchaser_pubkey: # if this is populated then this invoice was received
                inv_type = "received"
                pubkey = contract.purchaser_pubkey
            else: # otherwise this was sent
                inv_type = "sent"
                pubkey = contract.seedbox_pubkey

            invoices.append({
                'time': time_str,
                'amount': amount,
                'type': inv_type,
                'pubkey': pubkey,
            })
        context['invoices'] = invoices

        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

