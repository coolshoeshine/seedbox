from django import forms
from .models import UserContent

class UploadFileForm(forms.ModelForm):
    # file = forms.FileField()
    class Meta:
        model = UserContent
        fields = ['upload']
