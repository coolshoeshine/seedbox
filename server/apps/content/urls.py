from django.urls import path
from . import views

urlpatterns = [
    path('', views.ContentView.as_view(), name="content"),
    path('invoices', views.InvoicesView.as_view(), name="invoices"),
]
