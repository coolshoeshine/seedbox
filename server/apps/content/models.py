from datetime import datetime
from django.db import models
import os
from django.conf import settings
from django.utils.translation import gettext_lazy as _
import hashlib
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
from apps.websocket.models import SentMessage
import src.bin.node_info as nodes
import grpc
from grpc._channel import _InactiveRpcError
import json

class ContractStatus(models.TextChoices):
        PENDING = 'PEN', _("PENDING")
        ACTIVE = 'ACT', _("ACTIVE")
        EXPIRED = 'EXP', _("EXPIRED")
        CANCELLED = 'CAN', _("CANCELLED")

def create_contract_id(pubkey_a, pubkey_b, fee):
    dateTimeObj = datetime.now()
    hash_input = pubkey_a+pubkey_b+dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")+str(fee)
    hash_object = hashlib.sha256(bytes(hash_input, 'utf-8'))
    return hash_object.hexdigest()

class Contract(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    contract_id = models.CharField(max_length=255, unique=True, null=True)
    purchaser_pubkey = models.CharField(max_length=255, null=True)
    seedbox_pubkey = models.CharField(max_length=255, null=True)
    rate = models.FloatField(default=0) #MB/sats
    capacity = models.IntegerField(default=0) #bytes
    status = models.CharField(max_length=3, choices=ContractStatus.choices, default=ContractStatus.EXPIRED)
	
    content_name = models.CharField(max_length=255, null=True) # name of file
    path_to_file = models.CharField(max_length=500, null=True) # full file path
    file_size = models.FloatField(default=0) # MB
    total_uploaded = models.FloatField(default=0) # MB
    fees_paid = models.IntegerField(default=0) # sats
    magnet_link = models.CharField(max_length=255, null=True) # torrent magnet link
	
    def __str__(self):
        s = "%s" % (
            self.contract_id,
        )
        return s

    
    def AcceptOffer(self):
        send_dict = {
            'offer_accepted': True,
            'contract_id': self.contract_id,
        }
        msg = json.dumps(send_dict)
        send_message = msgrpc.SendMessageRequest(
            msg=msg,
            pubkey=self.seedbox_pubkey,
        )
        self.status = ContractStatus.ACTIVE
        self.save()

        # initialize websocket
        channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
        stub_msg = msgstub.MessagingStub(channel_imp)

        # send the message
        imp_response = stub_msg.SendMessage(send_message)

        # create a SentMessage object in database
        SentMessage.objects.using('localuser').create(
            msg=msg,
            pubkey=self.seedbox_pubkey,
            msg_id=imp_response.id,
        )


class ContractInvoices(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    contract = models.ForeignKey(Contract, null=True, on_delete=models.CASCADE)
    ln_req = models.CharField(max_length=300, unique=True)
    preimage = models.CharField(max_length=300, null=True)
    amount = models.IntegerField(default=0)
    attempt = models.IntegerField(default=0)




class UserContent(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # file info
    upload = models.FileField(upload_to='server/user_files/')
    name = models.CharField(max_length=255, null=True)
    size_bytes = models.IntegerField(default=0)
    size_kb = models.FloatField(default=0)
    size_mb = models.FloatField(default=0)
    full_path = models.CharField(max_length=255, null=True)

    #contract
    contract = models.ForeignKey(Contract, null=True, on_delete=models.CASCADE)

    def __str__(self):
        s = "%s" % (
            self.name,
        )
        return s

    
    def set_size(self):
        # query the os for the file size
        full_path = os.path.join(settings.BASE_DIR, "user_files", self.name)
        self.full_path = full_path
        file_size = os.path.getsize(full_path)
        
        # update the database object
        self.size_bytes = file_size
        self.size_kb = file_size / 2**10
        self.size_mb = file_size / 2**20
        self.save()
