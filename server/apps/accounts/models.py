from django.db import models
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
from apps.websocket.models import SentMessage
import src.bin.node_info as nodes
import grpc
from grpc._channel import _InactiveRpcError
import json
from apps.websocket.models import MethodOptions

# Create your models here.
class KnownOffers(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    pubkey = models.CharField(max_length=255, primary_key=True) # pubkey of that node

    # offer details (these WILL change over time, best practice is to re-query when searching for offers)
    disk_space = models.FloatField(default=0) # measured in GB
    upload_bandwidth = models.FloatField(default=0) # max KBPS upload rate
    cost_per_mb = models.IntegerField(default=0) # sats per MB

    # node status
    inactive = models.BooleanField(default=False) # True if we cannot query this node


    def __str__(self):
        s = "%s: %s GB with %s kbps upload for %s sats/mb" % (
            self.pubkey,
            self.disk_space,
            self.upload_bandwidth,
            self.cost_per_mb,
        )
        return s

    """
        function: query
        Will send a message to the imp node at pubkey asking for it's
        seedbox offering

        All this does is SEND the message
        Receiving the response information is left up to the
        websocket manager method (apps.websocket.views.websocket_test_datastream)
    """
    def query(self):
        try:
            # construct message
            msg_dict = {
                "method": MethodOptions.TorrentRequest,
            }
            msg = json.dumps(msg_dict)
            send_message = msgrpc.SendMessageRequest(
                msg=msg,
                pubkey=self.pubkey,
            )

            # initialize websocket
            channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
            stub_msg = msgstub.MessagingStub(channel_imp)

            # send the message
            imp_response = stub_msg.SendMessage(send_message)

            # create a SentMessage object in database
            SentMessage.objects.using('localuser').create(
                msg=msg,
                pubkey=self.pubkey,
                msg_id=imp_response.id,
            )

            print("  successfully sent message to ", self.pubkey)
        except _InactiveRpcError:
            print("  ERROR sending message to ", self.pubkey)
            self.inactive = True
            self.save()


