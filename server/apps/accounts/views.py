from django.shortcuts import render, redirect
from django.views import View
from .forms import LoginForm, RegisterForm
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login
import transmissionrpc
from django.http import StreamingHttpResponse
import time
import json
from django.core.serializers.json import DjangoJSONEncoder


class IndexView(View):
    template_name = "index.html"

    def get_context(self, request):
        context = {}
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)







class HelpView(View):
    template_name = "help.html"

    def get_context(self, request):
        context = {}
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

class WalletView(View):
    template_name = "wallet.html"

    def get_context(self, request):
        context = {}
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)





# the generator for the StreamingHttpResponse in TestStreamingResponseView
def test_event_stream():
    idx = 0
    data_list = []
    while True:
        data_list.append({'idx': idx})
        data = json.dumps(data_list, cls=DjangoJSONEncoder)

        yield "\ndata: %s\n\n" % data
        print(data)
        idx += 1
        time.sleep(2) # wait 2 seconds


# following this video:
# https://www.youtube.com/watch?v=iOV_vZW52bk
class TestStreamingResponseView(View):
    template_name = "test_streaming.html"

    def get_context(self, request):
        context = {}
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)



class TestDataStreamView(View):
    def get(self, request):
        response = StreamingHttpResponse(test_event_stream())
        response['Content-Type'] = 'text/event-stream'
        return response









class LoginView(View):
    template_name = 'login.html'

    def get_context(self, request):
        context = {}
        login_form = LoginForm(request.POST or None)
        context['login_form'] = login_form
        return context

    def get(self, request):
        auth.logout(request)
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)

        # parse the login form
        login_form = context['login_form']
        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            user_exists = User.objects.filter(username=username).exists()

            if user_exists:
                # log in and redirect to dashboard
                user = auth.authenticate(username=username, password=password)
                if user is not None:
                    auth.login(request, user)
                    return redirect('dashboard')

        # reset the login form
        context['login_form'] = LoginForm()
        return render(request, self.template_name, context)



class RegisterView(View):
    template_name = 'register.html'

    def get_context(self, request):
        context = {}
        register_form = RegisterForm(request.POST or None)
        context['register_form'] = register_form
        return context

    def get(self, request):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    def post(self, request):
        context = self.get_context(request)

        # parse the register form
        register_form = context['register_form']
        print(register_form)
        if register_form.is_valid():
            username = register_form.cleaned_data['username']
            password1 = register_form.cleaned_data['password1']
            password2 = register_form.cleaned_data['password2']
            if password1 == password2:
                user = User.objects.create_user(
                    username=username,
                    password=password1,
                )
                user.save()

                # log in and redirect to dashboard
                user = auth.authenticate(username=username, password=password1)
                if user is not None:
                    auth.login(request, user)
                    return redirect('dashboard')


                

        # reset the register form
        context['register_form'] = RegisterForm()
        return render(request, self.template_name, context)
