from . import views
from django.urls import path

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('help', views.HelpView.as_view(), name='help'),

    # these are just for test (we can delete later)
    path('test-streaming', views.TestStreamingResponseView.as_view(), name="test-streaming"),
    path('test-streaming-data', views.TestDataStreamView.as_view(), name="test-streaming-data"),
]
