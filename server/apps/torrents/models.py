from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Torrent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255) # user specified name for this torrent
    dht_magnet_hash = models.CharField(max_length=255) # magnet hash
    num_files = models.IntegerField(default=0) # number of files
    total_size = models.IntegerField(default=0) # size of all files in bytes
    magnet_link = models.CharField(max_length=255) # magnet link
    
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # payment info
    max_budget = models.IntegerField(default=0) # maximum budget in sats
    budget_spent = models.IntegerField(default=0) # sats spent so far
    budget_remaining = models.IntegerField(default=0) # max_budget - budget_spent

    # seeders
    num_seedboxes = models.IntegerField(default=0) # number of seedboxes seeding this torrent
    copies_seeded = models.IntegerField(default=0) # how many times has this been downloaded?


class File(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    torrent = models.ForeignKey(Torrent, on_delete=models.CASCADE)
    name = models.CharField(max_length=255) # user specified name for this file
    size = models.IntegerField(default=0) # size of the file in bytes
    
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)
