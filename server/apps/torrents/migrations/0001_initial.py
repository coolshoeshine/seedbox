# Generated by Django 3.1.5 on 2021-08-17 21:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Torrent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('dht_magnet_hash', models.CharField(max_length=255)),
                ('num_files', models.IntegerField(default=0)),
                ('total_size', models.IntegerField(default=0)),
                ('magnet_link', models.CharField(max_length=255)),
                ('last_updated', models.DateTimeField(auto_now=True, null=True)),
                ('time_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('max_budget', models.IntegerField(default=0)),
                ('budget_spent', models.IntegerField(default=0)),
                ('budget_remaining', models.IntegerField(default=0)),
                ('num_seedboxes', models.IntegerField(default=0)),
                ('copies_seeded', models.IntegerField(default=0)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('size', models.IntegerField(default=0)),
                ('last_updated', models.DateTimeField(auto_now=True, null=True)),
                ('time_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('torrent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='torrents.torrent')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
