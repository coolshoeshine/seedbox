"""
    file: SeedboxOffer.py

    Property of Cypher Enginering

    This script is a submodule meant to be used in the main
    websocket listener method
"""

#########################################################
## Imports

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# general python stuff
import json, os, time

#########################################################




"""
    function: SeedboxOffer
    Parses a received set of seedbox offer parameters and saves them 
    in the local database

    Parameters:
        rcvd_msg - type: apps.websocket.models.ReceivedMessage

                   desc: unparsed database object holding the data
                         received on the websocket

        response - type: string?

                   desc: the raw data received on the websocket
        
        stub_msg - type: src.dependencies.messaging_pb2_grpc.MessagingStub
                   
                   desc: grpc object to expose the messaging portion of
                         the impervious API
                         https://docs.impervious.ai/apis/messaging
                
        stub_web - type: src.dependencies.websocket_pb2_grpc.WebsocketStub
                   
                   desc: grpc object to expose the websocket portion of
                         the impervious API
                         https://docs.impervious.ai/apis/websocket
        
        stub_ln - type: src.dependencies.lightning_pb2_grpc.LightningStub

                  desc: grpc object to expose the Lightning service of
                        the LND node
                        https://api.lightning.community/#service-lightning
"""
def SeedboxOffer(rcvd_msg, response, stub_msg, stub_web, stub_ln):
    # unpack the json data from the response
    formatted_data = json.loads(response.data)
    formatted_offer_params = json.loads(formatted_data['offers'][0]['offer_parameters'])

    # extract data from the response
    pubkey = response.from_pubkey
    disk_space = formatted_offer_params['disk_space']
    upload_bandwidth = formatted_offer_params['upload_bandwidth']
    cost_per_mb = formatted_offer_params['cost_per_mb']
    contract_id = formatted_data['offers'][0]['contract_id']
    print("contract_id: ", contract_id)

    # save the offer data in the database (if it's defined)
    if disk_space > 0 and cost_per_mb > 0:
        offer_obj, was_created = KnownOffers.objects.using('localuser').get_or_create(pubkey=pubkey)
        offer_obj.disk_space = disk_space
        offer_obj.upload_bandwidth = upload_bandwidth
        offer_obj.cost_per_mb = cost_per_mb
        offer_obj.save()
        print(offer_obj)

        # create a Contract object too
        Contract.objects.using('localuser').get_or_create(
            contract_id=contract_id,
            seedbox_pubkey=response.from_pubkey,
            rate=cost_per_mb,
            capacity=disk_space,
            status=ContractStatus.PENDING,
        )