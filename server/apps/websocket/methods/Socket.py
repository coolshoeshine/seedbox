"""
    file: Socket.py

    Property of Cypher Enginering

    This script is a submodule meant to be used in the main
    websocket listener method
"""

#########################################################
## Imports

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# general python stuff
import json, os, time

# socket stuff
from src.config_files.client_config import Config
import socket

#########################################################




"""
    function: Socket
    Parses the UDP connection parameters and sends over the file
    with the associated contract id

    Parameters:
        rcvd_msg - type: apps.websocket.models.ReceivedMessage

                   desc: unparsed database object holding the data
                         received on the websocket

        response - type: string?

                   desc: the raw data received on the websocket
        
        stub_msg - type: src.dependencies.messaging_pb2_grpc.MessagingStub
                   
                   desc: grpc object to expose the messaging portion of
                         the impervious API
                         https://docs.impervious.ai/apis/messaging
                
        stub_web - type: src.dependencies.websocket_pb2_grpc.WebsocketStub
                   
                   desc: grpc object to expose the websocket portion of
                         the impervious API
                         https://docs.impervious.ai/apis/websocket
        
        stub_ln - type: src.dependencies.lightning_pb2_grpc.LightningStub

                  desc: grpc object to expose the Lightning service of
                        the LND node
                        https://api.lightning.community/#service-lightning
"""
def Socket(rcvd_msg, response, stub_msg, stub_web, stub_ln):
    print("=== response had 'socket'! Starting file transfer ===")

    # make sure it doesn't try to do this twice
    socket_status_obj, was_created = SocketStatus.objects.using('localuser').get_or_create(name="sending")
    if not socket_status_obj.in_use:
        socket_status_obj.in_use = True
        socket_status_obj.save()

        if json.loads(response.data)["socket"]["state"] == "start":
            host = json.loads(response.data)["socket"]["ip"]
            port = json.loads(response.data)["socket"]["port"]
            buf = json.loads(response.data)["socket"]["buf"]
            seperator = json.loads(response.data)["socket"]["separator"]
            contract_id = json.loads(response.data)["socket"]["contract_id"]

            # get contract object
            contract = Contract.objects.using('localuser').get(contract_id=contract_id)

            addr = (host, port)
            file_name = contract.content_name
            path_to_file = contract.path_to_file
            file_size = os.path.getsize(path_to_file)
            sock = socket.socket()
            print(f"[+] Connecting to {host}:{port}")
            time.sleep(5)
            sock.connect(addr)
            print(f"[+] Connected.")
            sock.send(f"{file_name}{seperator}{file_size}".encode())
            time.sleep(0.01)
            with open(path_to_file, "rb") as f:
                while True:
                    bytes_read = f.read(buf)
                    if not bytes_read:
                        break
                    sock.sendall(bytes_read)
            sock.close()
            print("[+] File sent")
    
    # reset the socket sending flag
    socket_status_obj.in_use = False
    socket_status_obj.save()