"""
    file: OfferAccepted.py

    Property of Cypher Enginering

    This script is a submodule meant to be used in the main
    websocket listener method
"""

#########################################################
## Imports

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# general python stuff
import json, os, time

# socket stuff
from src.config_files.client_config import Config
import socket

# torrent stuff
from src.tsm_rpc.make_torrent import make_torrent
from src.tsm_rpc.add_torrent import add_torrent
from src.tsm_rpc.get_torrents import get_torrents

#########################################################


config = Config()

"""
    function: OfferAccepted
    Opens up a UDP port and starts listening
    Sends the connection details to the client, and waits to receive a file

    Parameters:
        rcvd_msg - type: apps.websocket.models.ReceivedMessage

                   desc: unparsed database object holding the data
                         received on the websocket

        response - type: string?

                   desc: the raw data received on the websocket
        
        stub_msg - type: src.dependencies.messaging_pb2_grpc.MessagingStub
                   
                   desc: grpc object to expose the messaging portion of
                         the impervious API
                         https://docs.impervious.ai/apis/messaging
                
        stub_web - type: src.dependencies.websocket_pb2_grpc.WebsocketStub
                   
                   desc: grpc object to expose the websocket portion of
                         the impervious API
                         https://docs.impervious.ai/apis/websocket
        
        stub_ln - type: src.dependencies.lightning_pb2_grpc.LightningStub

                  desc: grpc object to expose the Lightning service of
                        the LND node
                        https://api.lightning.community/#service-lightning
"""
def OfferAccepted(rcvd_msg, response, stub_msg, stub_web, stub_ln):
    # make sure this doesn't try to open a listen socket twice
    socket_status_obj, was_created = SocketStatus.objects.using('localuser').get_or_create(name="listening")
    if not socket_status_obj.in_use:
        socket_status_obj.in_use = True
        socket_status_obj.save()

        #open connection via TCP
        contract = Contract.objects.using('localuser').get(
            contract_id=json.loads(response.data)['contract_id']
        )
        contract.status = ContractStatus.ACTIVE
        contract.save()
        server_host = config.ServerHost
        bind_to_address = '0.0.0.0'
        server_port = config.ServerPort
        separator = config.Separator
        print("trying to bind to: %s:%s" % (bind_to_address, server_port))
        buf = config.BufferSize
        s = socket.socket()
        s.bind((bind_to_address, server_port))
        s.listen(5)
        print(f"[+] Listening as {bind_to_address}:{server_port}")
        node_2_open = {
            "socket": {
                "state": "start",
                "ip": server_host,
                "port": server_port,
                "buf": buf,
                "separator": separator,
                "contract_id": contract.contract_id,
            }
        }
        msg = json.dumps(node_2_open)
        pubkey = response.from_pubkey
        open_req = msgrpc.SendMessageRequest(
            msg=msg,
            pubkey=pubkey
        )
        msg_resp = stub_msg.SendMessage(open_req)
        print("Message sent to open udp connection", msg_resp)
        client_socket, address = s.accept()
        print(f"[+] {address} is connected.")
        
        received = client_socket.recv(buf).decode()
        filename, filesize = received.split(separator)
        print("\nreceived: %s\n\n" % received)
        print("filename: ", filename)
        print("filesize: ", filesize)

        filename = os.path.basename(filename)
        filesize = int(filesize)

        with open(filename, "wb") as f:
            while True:
                bytes_read = client_socket.recv(buf)
                if not bytes_read:
                    break
                f.write(bytes_read)

        client_socket.close()
        s.close()
        
        #now connecting to bit torrent client
        # torrent_name = make_torrent(filename)
        # torrent = add_torrent(torrent_name)

        make_torrent(filename)
        add_torrent(filename)

        # list all torrents to get the torrent id
        torrents = get_torrents()
        torrent_id = -1
        for torrent in torrents:
            if torrent.name == filename:
                torrent_id = torrent.id
                print("found torrent id: ", filename, torrent_id)
                break

        seedbox_params = SeedBoxParameters.objects.using('localuser').get(pubkey="local")
        Seeder.objects.using('localuser').create(
            contract=contract,
            disk_space=seedbox_params.max_disk_space,
            upload_bandwidth=seedbox_params.maxUploadRate,
            cost_per_byte=seedbox_params.fee,
            torrent_id=torrent_id,
        )
    
    # delete the socket listening flag
    socket_status_obj.in_use = False
    socket_status_obj.save()