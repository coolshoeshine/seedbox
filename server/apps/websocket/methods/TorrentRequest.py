"""
    file: TorrentRequest.py

    Property of Cypher Enginering

    This script is a submodule meant to be used in the main
    websocket listener method
"""

#########################################################
## Imports

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# general python stuff
import json, os, time

#########################################################




"""
    function: TorrentRequest
    Fetches the user-defined seedbox parameters and responds with the seedbox
    offer info and a contract id

    Parameters:
        rcvd_msg - type: apps.websocket.models.ReceivedMessage

                   desc: unparsed database object holding the data
                         received on the websocket

        response - type: string?

                   desc: the raw data received on the websocket
        
        stub_msg - type: src.dependencies.messaging_pb2_grpc.MessagingStub
                   
                   desc: grpc object to expose the messaging portion of
                         the impervious API
                         https://docs.impervious.ai/apis/messaging
                
        stub_web - type: src.dependencies.websocket_pb2_grpc.WebsocketStub
                   
                   desc: grpc object to expose the websocket portion of
                         the impervious API
                         https://docs.impervious.ai/apis/websocket
        
        stub_ln - type: src.dependencies.lightning_pb2_grpc.LightningStub

                  desc: grpc object to expose the Lightning service of
                        the LND node
                        https://api.lightning.community/#service-lightning
"""
def TorrentRequest(rcvd_msg, response, stub_msg, stub_web, stub_ln):
    rcvd_offer_query = ReceivedOfferQuery.objects.using('localuser').create()
    rcvd_offer_query.__dict__.update(rcvd_msg.__dict__)
    # check for local user's seedbox parameters
    seedbox_settings, was_created = SeedBoxParameters.objects.using('localuser').get_or_create(pubkey="local")
    contract_id = create_contract_id(seedbox_settings.pubkey, response.from_pubkey, seedbox_settings.fee)
    msg_dict = {
            "offers": [
                {
                    "offer_parameters": seedbox_settings.offer_json(),
                    "contract_id": contract_id
                }
            ]
        }
    # respond to the querying node
    send_message = msgrpc.SendMessageRequest(
        msg=json.dumps(msg_dict),
        pubkey=response.from_pubkey,
    )
    imp_response = stub_msg.SendMessage(send_message)
    # create a SentMessage object in database
    SentMessage.objects.using('localuser').create(
        msg=seedbox_settings.offer_json(),
        pubkey=response.from_pubkey,
        msg_id=imp_response.id,
    )
    #create a Contract object in the database
    Contract.objects.using('localuser').create(
        contract_id=contract_id,
        purchaser_pubkey=response.from_pubkey,
        rate=seedbox_settings.fee,
        capacity=seedbox_settings.max_disk_space,
        status=ContractStatus.PENDING
    )


