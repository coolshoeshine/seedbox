"""
    file: Invoice.py

    Property of Cypher Enginering

    This script is a submodule meant to be used in the main
    websocket listener method
"""

#########################################################
## Imports

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# general python stuff
import json, os, time

#########################################################




"""
    function: Invoice
    Pays a lightning invoice associated with an active contract

    Parameters:
        rcvd_msg - type: apps.websocket.models.ReceivedMessage

                   desc: unparsed database object holding the data
                         received on the websocket

        response - type: string?

                   desc: the raw data received on the websocket
        
        stub_msg - type: src.dependencies.messaging_pb2_grpc.MessagingStub
                   
                   desc: grpc object to expose the messaging portion of
                         the impervious API
                         https://docs.impervious.ai/apis/messaging
                
        stub_web - type: src.dependencies.websocket_pb2_grpc.WebsocketStub
                   
                   desc: grpc object to expose the websocket portion of
                         the impervious API
                         https://docs.impervious.ai/apis/websocket
        
        stub_ln - type: src.dependencies.lightning_pb2_grpc.LightningStub

                  desc: grpc object to expose the Lightning service of
                        the LND node
                        https://api.lightning.community/#service-lightning
"""
def Invoice(rcvd_msg, response, stub_msg, stub_web, stub_ln):
    print("=== response had 'invoice'! Starting file transfer ===")
    contract = Contract.objects.using('localuser').get(
        contract_id=json.loads(response.data)['invoice']['contract_id']
    )
    pay_req = lnrpc.PayInvoiceRequest(
        invoice=json.loads(response.data)['invoice']['ln_req']
    )
    pay_resp = stub_ln.PayInvoice(pay_req)
    cont_inv = ContractInvoices.objects.using('localuser').create(
        contract=contract,
        ln_req=json.loads(response.data)['invoice']['ln_req'],
        preimage=pay_resp.preimage,
        amount=json.loads(response.data)['invoice']['amount']
    )