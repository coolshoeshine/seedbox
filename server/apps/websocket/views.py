from django.shortcuts import render
from django.views import View
from django.http import StreamingHttpResponse
import json
import time
import src.bin.node_info as nodes
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub
import grpc
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
import datetime
from .models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus
from apps.seeder.models import SeedBoxParameters
import socket
from src.config_files.client_config import Config
import os
from apps.accounts.models import KnownOffers
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.seeder.models import Seeder, SeedBoxParameters
from src.tsm_rpc.make_torrent import make_torrent
from src.tsm_rpc.add_torrent import add_torrent
from src.tsm_rpc.get_torrents import get_torrents

config = Config()

# Create your views here.
class WebsocketTestView(View):
    template_name = "websocket_test.html"

    def get(self, request):
        context = {}
        return render(request, self.template_name, context)















"""
    function: websocket_test_datastream
    This started as a test, but is quickly become the main websocket manager function.

    This is a generator function which will yield a response whenever a new message
    comes in on the websocket

    Meant to be used with a StreamingHttpResponse object in a view
"""
def websocket_test_datastream():
    # initial yield
    all_messages = ReceivedMessage.objects.using('localuser').all().order_by("-time_created")
    data = serialize('json', all_messages, cls=DjangoJSONEncoder)
    yield "\ndata: %s\n\n" % data

    # initialize websocket
    channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
    stub_msg = msgstub.MessagingStub(channel_imp)
    stub_web = webstub.WebsocketStub(channel_imp)
    stub_ln = lnstub.LightningStub(channel_imp)

    # listen on websocket
    request = webrpc.SubscribeRequest()
    for response in stub_web.Subscribe(request):
        print(response)

        # create database object
        rcvd_msg = ReceivedMessage.objects.using('localuser').create(
            msg_id=response.id,
            from_pubkey=response.from_pubkey,
            data=response.data,
            service_type=response.service_type,
            amount=response.amount,
        )
        print(rcvd_msg)

        # check if this message was querying for a torrent offer
        if response.data == "torrent_request":
            rcvd_offer_query = ReceivedOfferQuery.objects.using('localuser').create()
            rcvd_offer_query.__dict__.update(rcvd_msg.__dict__)
            # check for local user's seedbox parameters
            seedbox_settings, was_created = SeedBoxParameters.objects.using('localuser').get_or_create(pubkey="local")
            contract_id = create_contract_id(seedbox_settings.pubkey, response.from_pubkey, seedbox_settings.fee)
            msg_dict = {
                    "offers": [
                        {
                            "offer_parameters": seedbox_settings.offer_json(),
                            "contract_id": contract_id
                        }
                    ]
                }
            # respond to the querying node
            send_message = msgrpc.SendMessageRequest(
                msg=json.dumps(msg_dict),
                pubkey=response.from_pubkey,
            )
            imp_response = stub_msg.SendMessage(send_message)
            # create a SentMessage object in database
            SentMessage.objects.using('localuser').create(
                msg=seedbox_settings.offer_json(),
                pubkey=response.from_pubkey,
                msg_id=imp_response.id,
            )
            #create a Contract object in the database
            Contract.objects.using('localuser').create(
                contract_id=contract_id,
                purchaser_pubkey=response.from_pubkey,
                rate=seedbox_settings.fee,
                capacity=seedbox_settings.max_disk_space,
                status=ContractStatus.PENDING
            )

        
        elif "offer_accepted" in json.loads(response.data):
            # make sure this doesn't try to open a listen socket twice
            socket_status_obj, was_created = SocketStatus.objects.using('localuser').get_or_create(name="listening")
            if not socket_status_obj.in_use:
                socket_status_obj.in_use = True
                socket_status_obj.save()

                #open connection via TCP
                contract = Contract.objects.using('localuser').get(
                    contract_id=json.loads(response.data)['contract_id']
                )
                contract.status = ContractStatus.ACTIVE
                contract.save()
                server_host = config.ServerHost
                bind_to_address = '0.0.0.0'
                server_port = config.ServerPort
                separator = config.Separator
                print("trying to bind to: %s:%s" % (bind_to_address, server_port))
                buf = config.BufferSize
                s = socket.socket()
                s.bind((bind_to_address, server_port))
                s.listen(5)
                print(f"[+] Listening as {bind_to_address}:{server_port}")
                node_2_open = {
                    "socket": {
                        "state": "start",
                        "ip": server_host,
                        "port": server_port,
                        "buf": buf,
                        "separator": separator,
                        "contract_id": contract.contract_id,
                    }
                }
                msg = json.dumps(node_2_open)
                pubkey = response.from_pubkey
                open_req = msgrpc.SendMessageRequest(
                    msg=msg,
                    pubkey=pubkey
                )
                msg_resp = stub_msg.SendMessage(open_req)
                print("Message sent to open udp connection", msg_resp)
                client_socket, address = s.accept()
                print(f"[+] {address} is connected.")
                
                received = client_socket.recv(buf).decode()
                filename, filesize = received.split(separator)
                print("\nreceived: %s\n\n" % received)
                print("filename: ", filename)
                print("filesize: ", filesize)

                filename = os.path.basename(filename)
                filesize = int(filesize)

                with open(filename, "wb") as f:
                    while True:
                        bytes_read = client_socket.recv(buf)
                        if not bytes_read:
                            break
                        f.write(bytes_read)

                client_socket.close()
                s.close()
                
                #now connecting to bit torrent client
                # torrent_name = make_torrent(filename)
                # torrent = add_torrent(torrent_name)

                make_torrent(filename)
                add_torrent(filename)

                # list all torrents to get the torrent id
                torrents = get_torrents()
                torrent_id = -1
                for torrent in torrents:
                    if torrent.name == filename:
                        torrent_id = torrent.id
                        print("found torrent id: ", filename, torrent_id)
                        break

                seedbox_params = SeedBoxParameters.objects.using('localuser').get(pubkey="local")
                Seeder.objects.using('localuser').create(
                    contract=contract,
                    disk_space=seedbox_params.max_disk_space,
                    upload_bandwidth=seedbox_params.maxUploadRate,
                    cost_per_byte=seedbox_params.fee,
                    torrent_id=torrent_id,
                )
            
            # delete the socket listening flag
            socket_status_obj.in_use = False
            socket_status_obj.save()


        # check if this message is a seedbox_offer response
        elif "offers" in json.loads(response.data):
            # unpack the json data from the response
            formatted_data = json.loads(response.data)
            formatted_offer_params = json.loads(formatted_data['offers'][0]['offer_parameters'])

            # extract data from the response
            pubkey = response.from_pubkey
            disk_space = formatted_offer_params['disk_space']
            upload_bandwidth = formatted_offer_params['upload_bandwidth']
            cost_per_mb = formatted_offer_params['cost_per_mb']
            contract_id = formatted_data['offers'][0]['contract_id']
            print("contract_id: ", contract_id)

            # save the offer data in the database (if it's defined)
            if disk_space > 0 and cost_per_mb > 0:
                offer_obj, was_created = KnownOffers.objects.using('localuser').get_or_create(pubkey=pubkey)
                offer_obj.disk_space = disk_space
                offer_obj.upload_bandwidth = upload_bandwidth
                offer_obj.cost_per_mb = cost_per_mb
                offer_obj.save()
                print(offer_obj)

                # create a Contract object too
                Contract.objects.using('localuser').get_or_create(
                    contract_id=contract_id,
                    seedbox_pubkey=response.from_pubkey,
                    rate=cost_per_mb,
                    capacity=disk_space,
                    status=ContractStatus.PENDING,
                )
        
        # if "socket" is in the response, send the file over
        elif "socket" in json.loads(response.data):
            print("=== response had 'socket'! Starting file transfer ===")

            # make sure it doesn't try to do this twice
            socket_status_obj, was_created = SocketStatus.objects.using('localuser').get_or_create(name="sending")
            if not socket_status_obj.in_use:
                socket_status_obj.in_use = True
                socket_status_obj.save()

                if json.loads(response.data)["socket"]["state"] == "start":
                    host = json.loads(response.data)["socket"]["ip"]
                    port = json.loads(response.data)["socket"]["port"]
                    buf = json.loads(response.data)["socket"]["buf"]
                    seperator = json.loads(response.data)["socket"]["separator"]
                    contract_id = json.loads(response.data)["socket"]["contract_id"]

                    # get contract object
                    contract = Contract.objects.using('localuser').get(contract_id=contract_id)

                    addr = (host, port)
                    file_name = contract.content_name
                    path_to_file = contract.path_to_file
                    file_size = os.path.getsize(path_to_file)
                    sock = socket.socket()
                    print(f"[+] Connecting to {host}:{port}")
                    time.sleep(5)
                    sock.connect(addr)
                    print(f"[+] Connected.")
                    sock.send(f"{file_name}{seperator}{file_size}".encode())
                    time.sleep(0.01)
                    with open(path_to_file, "rb") as f:
                        while True:
                            bytes_read = f.read(buf)
                            if not bytes_read:
                                break
                            sock.sendall(bytes_read)
                    sock.close()
                    print("[+] File sent")
            
            # reset the socket sending flag
            socket_status_obj.in_use = False
            socket_status_obj.save()

        elif "invoice" in json.loads(response.data):
            print("=== response had 'invoice'! Starting file transfer ===")
            contract = Contract.objects.using('localuser').get(
                contract_id=json.loads(response.data)['invoice']['contract_id']
            )
            pay_req = lnrpc.PayInvoiceRequest(
                invoice=json.loads(response.data)['invoice']['ln_req']
            )
            pay_resp = stub_ln.PayInvoice(pay_req)
            cont_inv = ContractInvoices.objects.using('localuser').create(
                contract=contract,
                ln_req=json.loads(response.data)['invoice']['ln_req'],
                preimage=pay_resp.preimage,
                amount=json.loads(response.data)['invoice']['amount']
            )


        # pull all messages from database
        all_messages = ReceivedMessage.objects.using('localuser').all().order_by("-time_created")
        data = serialize('json', all_messages, cls=DjangoJSONEncoder)
        yield "\ndata: %s\n\n" % data


class WebsocketDataStreamView(View):
    def get(self, request):
        response = StreamingHttpResponse(websocket_test_datastream())
        response['Content-Type'] = 'text/event-stream'
        return response
