from django.db import models



"""
    class: MethodOptions

"""
class MethodOptions(models.TextChoices):
    # if no 'method' field in the received message
    NA = "NA", "NA"

    # legacy options (pre Sep 13, 2021)
    TorrentRequest = "TorrentRequest", "Torrent Request"
    OfferAccepted = "OfferAccepted", "Offer Accepted"
    SeedboxOffer = "SeedboxOffer", "Seedbox Offer"
    Socket = "Socket", "Socket"
    Invoice = "Invoice", "Invoice"

    # newly created options (post Sep 13, 2021)







"""
    class: ReceivedMessage
    Keeps a record of ALL messages received on the websocket
"""
class ReceivedMessage(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # info from the message
    msg_id = models.CharField(max_length=255) # can't name id or else it will conflict with django built-in id field
    from_pubkey = models.CharField(max_length=255) # pubkey of node that sent this message
    data = models.CharField(max_length=255) # the actual message data
    service_type = models.CharField(max_length=255) # fixed to "message" I guess
    amount = models.IntegerField(default=0) # sats spent to send this message
    method = models.CharField(
        max_length=100,
        choices=MethodOptions.choices,
        default=MethodOptions.NA,
    )


    def __str__(self):
        s = "%s: %s" % (
            self.msg_id,
            self.data,
        )
        return s
    


"""
    class: ReceivedOfferQuery
    child class of <ReceivedMessage>

    Is only created if data == "torrent_request"
"""
class ReceivedOfferQuery(ReceivedMessage):
    pass



"""
    class: SentMessage
    Keeps a record of ALL messages sent from this node
"""
class SentMessage(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    # msg stuff
    msg = models.CharField(max_length=255) # actual message text
    pubkey = models.CharField(max_length=255) # pubkey this message was sent to
    msg_id = models.CharField(max_length=255) # can't name id or else it will conflict with django built-in id field




"""
    class: SocketStatus
    Used to prevent duplicate socket openings in the websocket method
"""
class SocketStatus(models.Model):
    # timing stuff
    last_updated = models.DateTimeField(auto_now=True, null=True)
    time_created = models.DateTimeField(auto_now_add=True, null=True)

    name = models.CharField(max_length=255, primary_key=True)
    in_use = models.BooleanField(default=False)


