from django.urls import path
from . import views

urlpatterns = [
    path('test', views.WebsocketTestView.as_view(), name="websocket-test"),
    path('test-datastream', views.WebsocketDataStreamView.as_view(), name="websocket-test-datastream"),
]
