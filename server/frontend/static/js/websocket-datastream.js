
var eventSource = new EventSource("/websocket/test-datastream");

eventSource.onopen = function () {
    console.log("we have an open connection!");
}

eventSource.onmessage = function(e) {
    console.log(e);
    parsed_data = JSON.parse(e.data);
    content = "";
    parsed_data.forEach(function(item) {
        content += buildContent(
            item['fields']['msg_id'],
            item['fields']['from_pubkey'],
            item['fields']['data'],
            item['fields']['service_type'],
            item['fields']['amount'],
            item['fields']['time_created'],
            );
    });

    document.getElementById('stream-content').innerHTML = content;

}

eventSource.onerror = function(e) {
    console.log('error: ', e);
}

function buildContent(id, from_pubkey, msg_data, service_type, amount, timestamp) {
    let content = `
    <p>time: ${timestamp}</p>
    <p>id: ${id}</p>
    <p>from_pubkey: ${from_pubkey}</p>
    <p>data: ${msg_data}</p>
    <p>service_type: ${service_type}</p>
    <p>amount: ${amount}</p>
    <hr>`;
    return content
}
