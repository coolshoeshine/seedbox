if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage
from apps.seeder.models import SeedBoxParameters
from apps.accounts.models import KnownOffers
from apps.content.models import UserContent


def reset_localuser():
    print("=== Deleting ALL data in local database localuser_db.sqlite3 ===")

    print("Deleting all ReceivedMessage objects")
    ReceivedOfferQuery.objects.using('localuser').all().delete()
    ReceivedMessage.objects.using('localuser').all().delete()

    print("Deleting all SentMessage objects")
    SentMessage.objects.using('localuser').all().delete()

    print("Deleting all SeedBoxParameters objects")
    SeedBoxParameters.objects.using('localuser').all().delete()

    print("Deleting all KnownOffers objects")
    KnownOffers.objects.using('localuser').all().delete()

    print("Deleting all UserContent objects")
    UserContent.objects.using('localuser').all().delete()



if __name__ == "__main__":
    reset_localuser()
