from requests import get

public_ip = get('https://api.ipify.org').text
print('My public IP address is: {}'.format(public_ip))

class Config():
    # ServerHost="127.0.0.1"
    ServerHost=public_ip
    ServerPort=6000
    Separator="<SEPARATOR>"
    BufferSize=4096