if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()

import dependencies.messaging_pb2 as msgrpc
import dependencies.messaging_pb2_grpc as msgstub
import dependencies.socket_pb2 as sockrpc
import dependencies.socket_pb2_grpc as sockstub
import dependencies.websocket_pb2 as webrpc
import dependencies.websocket_pb2_grpc as webstub
import grpc
import json
import os
import socket
import sys
import time

channel_imp = grpc.insecure_channel('54.193.121.26:8881')
stub_msg = msgstub.MessagingStub(channel_imp)
stub_web = webstub.WebsocketStub(channel_imp)
stub_sock = sockstub.SocketStub(channel_imp)



msg = "torrent_request"
pubkey = "025e1ef29516bb69c8de0b041c77c54b1eb46ff4ed566daf0d8bfa04aafe11b486"
request = msgrpc.SendMessageRequest(
    msg=msg,
    pubkey=pubkey,
)
response = stub_msg.SendMessage(request)
print(response.id)
request = webrpc.SubscribeRequest()
response = stub_web.Subscribe(request)
for resp in response:
    print(resp)
    if resp.service_type == "message":
        if "offer_parameters" in json.loads(resp.data):
            msg = { 
                "offer_accepted": {
                    "contract_id": json.loads(resp.data)['contract_id']
                }
            }
            pubkey = "025e1ef29516bb69c8de0b041c77c54b1eb46ff4ed566daf0d8bfa04aafe11b486"
            msg_req = msgrpc.SendMessageRequest(
                msg=msg,
                pubkey=pubkey,
            )
            msg_resp = stub_msg.SendMessage(msg_req)
            print(msg_resp.id)
        elif "socket" in json.loads(resp.data):
            if json.loads(resp.data)["socket"]["state"] == "start":
                host = json.loads(resp.data)["socket"]["ip"]
                port = json.loads(resp.data)["socket"]["port"]
                buf = json.loads(resp.data)["socket"]["buf"]
                seperator = json.loads(resp.data)["socket"]["separator"]
                addr = (host, port)
                file_name = sys.argv[1]
                file_size = os.path.getsize(file_name)
                sock = socket.socket()
                print(f"[+] Connecting to {host}:{port}")
                time.sleep(5)
                sock.connect(addr)
                print(f"[+] Connected.")
                sock.send(f"{file_name}{seperator}{file_size}".encode())
                with open(file_name, "rb") as f:
                    while True:
                        bytes_read = f.read(buf)
                        if not bytes_read:
                            break
                        sock.sendall(bytes_read)
                sock.close()
                print("[+] File sent")
                break