if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
import asyncio
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub
import grpc
import json

channel_imp = grpc.insecure_channel('54.67.50.223:8881')
stub_msg = msgstub.MessagingStub(channel_imp)
stub_web = webstub.WebsocketStub(channel_imp)
# msg = "Hello World"
# pubkey = "02c5fb01ccff281e827885f2d9faea4713e875b0b968a8b0550ab03109c9c1d451"
# request = msgrpc.SendMessageRequest(
#     msg=msg,
#     pubkey=pubkey,
# )
# response = stub_imp.SendMessage(request)
# print(response.id)
request = webrpc.SubscribeRequest()
for response in stub_web.Subscribe(request):
    print(response)
    if response.data == "torrent_request":
        offer = {
            "disk_space": 500000000000,
            "upload_bandwith": 10000000,
            "cost_per_byte": 1
        }
        msg = json.dumps(offer)
        pubkey = response.from_pubkey
        req = msgrpc.SendMessageRequest(
            msg=msg,
            pubkey=pubkey,
        )
        response = stub_msg.SendMessage(req)
        print(response.id)
