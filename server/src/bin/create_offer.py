import dependencies.messaging_pb2 as msgrpc
import dependencies.messaging_pb2_grpc as msgstub
import dependencies.signing_pb2 as signrpc
import dependencies.signing_pb2_grpc as signstub
import grpc

# Workflow
# user clicks on new account button
# they give node pubkey, username and password (to be stored locally so someone can have multiple nodes and accounts)
# 
# Workflow
# user clicks on make offer
# alice -> coordinator: signal intent to make an offer
# coordinator -> alice sign this JWT token
# alice -> coordinator sends signed token
# coordinator verifies signature
# 
# alice -> coordinator messages: pubkey, tx details
# coordinator lists offer
# coordinator -> alice: message detailing success
# 

