"""
    cron job (run every minute)
    */1 * * * * /usr/bin/python3 /home/ubuntu/seedbox/server/src/cronjobs/seed_upload.py >> /home/ubuntu/cron.log 2>&1
"""

if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    sys.path.append("/home/ubuntu/seedbox/server") # necessary for cron job
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import grpc
from apps.content.models import Contract, ContractStatus, ContractInvoices
from apps.seeder.models import Seeder
import src.bin.node_info as nodes
import transmissionrpc
from src.tsm_rpc.get_torrent import get_torrent
from src.tsm_rpc.start_torrent import start_torrent
from src.tsm_rpc.stop_torrent import stop_torrent
import json
import datetime
from src.cronjobs.common import log_print



def seed_upload():
    log_print('seed_upload', 'starting')
    #to be populated with BitTorrent client checker stuff
    #then update Seeder table with new info only if associated contract status is active
    #if contract isn't active, seeding is stopped via BitTorrent RPC
    # Would be nice is client pulled info inputted by the user from the GUI into a db table that we read from
    contracts = Contract.objects.using('localuser').all()
    for contract in contracts:
        seeders =Seeder.objects.using('localuser').all().filter(
            contract=contract
        )
        for seeder in seeders:
            torrent_file = get_torrent(seeder.torrent_id)
            if torrent_file:
                log_print('seed_upload', 'torrent_file: %s' % torrent_file)
                log_print('seed_upload', 'torrent_file_status: %s' % torrent_file.status)
                if torrent_file.status == 'seeding': #if it's currently seeding
                    log_print('seed_upload', '\ttorrent_id %s is currently seeding' % seeder.torrent_id)
                    if contract.status != ContractStatus.ACTIVE: #and the contract isn't active
                        stop_torrent(seeder.torrent_id)
                    else: #the contract is active
                        total_bytes = torrent_file.uploadedEver
                        log_print('seed_upload', '\ttotal_bytes: %s' % total_bytes)
                        log_print('seed_upload', '\tbytes_seeded: %s' % seeder.bytes_seeded)

                        seeder.bytes_delta = total_bytes - seeder.bytes_seeded
                        seeder.mb_delta = seeder.bytes_delta / 2**20 # convert from bytes to mb
                        seeder.bytes_seeded = total_bytes
                        seeder.save()


                elif contract.status == ContractStatus.ACTIVE: #it's not seeding but the contract is active
                    start_torrent(seeder.torrent_id)
            else:
                log_print('seed_upload', 'seeder:\n\t%s\n\tdid not have a matching torrent!' % seeder)



if __name__ == "__main__":
    seed_upload()