import datetime


def log_print(method_name, msg):
    msg_to_print = "[%s %s] %s" % (
        datetime.datetime.now(),
        method_name,
        msg,
    )
    print(msg_to_print)


