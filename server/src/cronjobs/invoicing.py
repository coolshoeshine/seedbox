"""
    cron job (run every minute)
    */1 * * * * /usr/bin/python3 /home/ubuntu/seedbox/server/src/cronjobs/invoicing.py >> /home/ubuntu/cron.log 2>&1
"""

if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    sys.path.append("/home/ubuntu/seedbox/server") # necessary for cron job
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import grpc
from apps.content.models import Contract, ContractStatus, ContractInvoices
from apps.seeder.models import Seeder
import src.bin.node_info as nodes
import transmissionrpc
from src.tsm_rpc.get_torrent import get_torrent
from src.tsm_rpc.start_torrent import start_torrent
from src.tsm_rpc.stop_torrent import stop_torrent
import json
import datetime
from src.cronjobs.common import log_print

def generate_and_send_invoice(stub_ln, stub_msg, contract, update_attemps=False):
    log_print('generate_and_send_invoice', 'starting')
    seederObjs = Seeder.objects.using('localuser').all().filter(
            contract=contract
        )
    amt = 0
    for seeder in seederObjs:
        amt += seeder.mb_delta*seeder.cost_per_byte # (cost_per_byte is actually sats/mb)
    
    # only continue if amt > 0
    if int(amt) <= 0:
        return

    dateTimeObj = datetime.datetime.now()
    inv_req = lnrpc.GenerateInvoiceRequest(
        amount=int(amt),
        memo=f'Seeding invoice {dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")}'
    )
    inv = stub_ln.GenerateInvoice(inv_req)
    inv_msg = {
        "invoice": {
            "contract_id": contract.contract_id,
            "ln_req": inv.invoice,
            "amount": amt
        }
    }
    log_print(
        'generate_and_send_invoice', 
        'inv_msg: %s' % inv_msg,
    )
    msg = json.dumps(inv_msg)
    req = msgrpc.SendMessageRequest(
        msg=msg,
        pubkey=contract.purchaser_pubkey,
    )
    msg_resp = stub_msg.SendMessage(req)
    if not update_attemps:
        ContractInvoices.objects.using('localuser').create(
            contract=contract,
            ln_req=inv.invoice,
            amount=amt
        )
    else:
        cont_inv = ContractInvoices.objects.using('localuser').get(
            contract=contract
        )
        cont_inv.ln_req = inv.invoice
        cont_inv.attempt = 1
        cont_inv.amount = amt
        cont_inv.save()

def invoicing():
    log_print('invoicing', 'starting')
    channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
    stub_ln = lnstub.LightningStub(channel_imp)
    stub_msg = msgstub.MessagingStub(channel_imp)
    
    contracts = Contract.objects.using('localuser').all().filter(
        status=ContractStatus.ACTIVE
    )
    for contract in contracts:
        generate_and_send_invoice(stub_ln, stub_msg, contract)


if __name__ == "__main__":
    invoicing()
