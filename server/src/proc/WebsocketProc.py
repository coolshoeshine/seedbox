"""
    file: WebsocketProc.py

    Property of Cypher Enginering

"""
#########################################################
## Imports

# this stuff is required to use django stuff without going through manage.py
if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    # sys.path.append("/home/paul/seedbox/server") # necessary for cron job
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()

# grpc stuff
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub

# node info
import src.bin.node_info as nodes

# other django apps/models
from apps.websocket.models import ReceivedMessage, ReceivedOfferQuery, SentMessage, SocketStatus, MethodOptions
from apps.seeder.models import Seeder, SeedBoxParameters
from apps.content.models import Contract, ContractStatus, create_contract_id, ContractInvoices
from apps.accounts.models import KnownOffers

# method scripts
from apps.websocket.methods.TorrentRequest import TorrentRequest
from apps.websocket.methods.OfferAccepted import OfferAccepted
from apps.websocket.methods.SeedboxOffer import SeedboxOffer
from apps.websocket.methods.Socket import Socket
from apps.websocket.methods.Invoice import Invoice

# general python stuff
import json, os, time, grpc

#########################################################




"""
    function: WebsocketProc
    This is the main Websocket Process method
    Meant to run forever, alongside the django server
    (in a separate terminal)

    This will subscribe to the impervious web socket and respond
    to all incoming messages

    Messages are expected to be json formatted
    and have a 'method' parameter
    See <apps.websocket.models.MethodOptions>

    In seedbox, Start with:

    python server/src/proc/WebsocketProc.py

    python3 server/src/proc/WebsocketProc.py
"""
def WebsocketProc():
    print("=== Starting WebsocketProc ===")

    # initialize websocket
    channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
    stub_msg = msgstub.MessagingStub(channel_imp)
    stub_web = webstub.WebsocketStub(channel_imp)
    stub_ln = lnstub.LightningStub(channel_imp)

    # listen on websocket
    request = webrpc.SubscribeRequest()
    for response in stub_web.Subscribe(request):
        print(response)

        # create database object
        rcvd_msg = ReceivedMessage.objects.using('localuser').create(
            msg_id=response.id,
            from_pubkey=response.from_pubkey,
            data=response.data,
            service_type=response.service_type,
            amount=response.amount,
        )
        print(rcvd_msg)

        # json parse the received data
        clean_data = json.loads(response.data)

        # handle the message method
        if 'method' in clean_data:
            method = clean_data['method']
            if method in MethodOptions:
                rcvd_msg.method = method
                rcvd_msg.save()

                if method == MethodOptions.TorrentRequest:
                    TorrentRequest(rcvd_msg, response, stub_msg, stub_web, stub_ln)
                elif method == MethodOptions.OfferAccepted:
                    OfferAccepted(rcvd_msg, response, stub_msg, stub_web, stub_ln)
                elif method == MethodOptions.SeedboxOffer:
                    SeedboxOffer(rcvd_msg, response, stub_msg, stub_web, stub_ln)
                elif method == MethodOptions.Socket:
                    Socket(rcvd_msg, response, stub_msg, stub_web, stub_ln)
                elif method == MethodOptions.Invoice:
                    Invoice(rcvd_msg, response, stub_msg, stub_web, stub_ln)
        
        # if no 'method' is defined, handle with the legacy options
        else:
            if response.data == "torrent_request":
                # TorrentRequest(rcvd_msg, response, stub_msg, stub_web, stub_ln)
                # This is only here still to explain how the legacy stuff worked
                raise Exception("this should have never happened, functionality was intended to be removed")
            elif "offer_accepted" in json.loads(response.data):
                OfferAccepted(rcvd_msg, response, stub_msg, stub_web, stub_ln)
            elif "offers" in json.loads(response.data):
                SeedboxOffer(rcvd_msg, response, stub_msg, stub_web, stub_ln)
            elif "socket" in json.loads(response.data):
                Socket(rcvd_msg, response, stub_msg, stub_web, stub_ln)
            elif "invoice" in json.loads(response.data):
                Invoice(rcvd_msg, response, stub_msg, stub_web, stub_ln)





if __name__ == "__main__":
    WebsocketProc()

