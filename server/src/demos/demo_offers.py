if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
from apps.accounts.models import KnownOffers



def demo_offers():
    # delete all existing offers
    KnownOffers.objects.using('localuser').all().delete()

    # make an offer
    KnownOffers.objects.using('localuser').create(
        pubkey="pubkey01",
        disk_space=500, # GB
        upload_bandwidth=1000, # kbps
        cost_per_mb = 1, # sats/mb
    )

    # make an offer
    KnownOffers.objects.using('localuser').create(
        pubkey="pubkey02",
        disk_space=500, # GB
        upload_bandwidth=1000, # kbps
        cost_per_mb = 1, # sats/mb
    )

    # make an offer
    KnownOffers.objects.using('localuser').create(
        pubkey="pubkey03",
        disk_space=0.000000001240, # GB
        upload_bandwidth=1000, # kbps
        cost_per_mb = 1, # sats/mb
    )




if __name__ == "__main__":
    demo_offers()


