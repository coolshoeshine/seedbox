if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
from apps.content.models import Contract, ContractStatus, ContractInvoices


def demo_contracts():
    Contract.objects.using('localuser').all().delete()
    ContractInvoices.objects.using('localuser').all().delete()

    contract01 = Contract.objects.using('localuser').create(
        contract_id='123456789123456789',
        purchaser_pubkey='pubkey01',
        rate=1.0,
        capacity=1000000000, # bytes
        status=ContractStatus.ACTIVE,
    )

    ContractInvoices.objects.using('localuser').create(
        contract=contract01,
        amount=420,
        ln_req="abcd01",
    )


    contract02 = Contract.objects.using('localuser').create(
        contract_id='ABC456789123456789',
        purchaser_pubkey='pubkey02',
        rate=2.0,
        capacity=1000000000, # bytes
        status=ContractStatus.PENDING,
    )

    ContractInvoices.objects.using('localuser').create(
        contract=contract02,
        amount=69,
        ln_req="abcd02",
    )



    contract03 = Contract.objects.using('localuser').create(
        contract_id='ABCDEF789123456789',
        seedbox_pubkey='pubkey03',
        rate=3.0,
        capacity=1000000000, # bytes
        status=ContractStatus.EXPIRED,
    )

    ContractInvoices.objects.using('localuser').create(
        contract=contract03,
        amount=420,
        ln_req="abcd03",
    )


if __name__ == "__main__":
    demo_contracts()
