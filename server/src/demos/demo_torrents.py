torrents = []

torrents.append({
    "id": 1,
    "name": "Bitcoin White Paper",
    "magnetLink": "magnetLink",
    "sizeWhenDone": 2, # MB
    "rateDownload": 15, # kbps
    "downloadedEver": 56, # kb
    "rateUpload": 200, # kbps
    "uploadedEver": 8, # mb
    "uploadRatio": 4.1,
    "earnings": 25, # in sats
    "statusBar_type": "info",
    "statusBar_width": 45, # %
    "statusBar_text": "Downloading",
})

torrents.append({
    "id": 2,
    "name": "Bitcoin Core Binaries",
    "magnetLink": "magnetLink",
    "sizeWhenDone": 3, # MB
    "rateDownload": 16, # kbps
    "downloadedEver": 57, # kb
    "rateUpload": 201, # kbps
    "uploadedEver": 9, # mb
    "uploadRatio": 4.2,
    "earnings": 5, # in sats
    "statusBar_type": "success",
    "statusBar_width": 100, # %
    "statusBar_text": "Seeding",
})

torrents.append({
    "id": 3,
    "name": "Hillary's Emails",
    "magnetLink": "magnetLink",
    "sizeWhenDone": 4, # MB
    "rateDownload": 17, # kbps
    "downloadedEver": 58, # kb
    "rateUpload": 202, # kbps
    "uploadedEver": 10, # mb
    "uploadRatio": 4.3,
    "earnings": 500, # in sats
    "statusBar_type": "warning",
    "statusBar_width": 75, # %
    "statusBar_text": "Warning",
})

torrents.append({
    "id": 4,
    "name": "Cicada 3301 Music",
    "magnetLink": "magnetLink",
    "sizeWhenDone": 5, # MB
    "rateDownload": 18, # kbps
    "downloadedEver": 59, # kb
    "rateUpload": 203, # kbps
    "uploadedEver": 11, # mb
    "uploadRatio": 4.4,
    "earnings": 25, # in sats
    "statusBar_type": "danger",
    "statusBar_width": 50, # %
    "statusBar_text": "Error",
})
