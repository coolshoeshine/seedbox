import transmissionrpc

"""
    function: get_torrent
    Returns info on a single torrent

    See: https://github.com/0vercl0k/stuffz/blob/master/transmissionrpc.py
"""
def stop_torrent(torrent_to_stop):
    client = transmissionrpc.Client('127.0.0.1', port=9091, user='admin', password='admin')
    torrent = client.stop_torrent(torrent_to_stop)