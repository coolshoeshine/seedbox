import transmissionrpc
import sys

"""
    function: get_torrents
    Returns a list of all torrents

    See: https://github.com/0vercl0k/stuffz/blob/master/transmissionrpc.py
"""
def get_torrents():
    try:
        client = transmissionrpc.Client('127.0.0.1', port=9091, user='admin', password='admin')
        torrents = client.get_torrents()
        return torrents
    except:
        print("No connection to transmissionrpc!")
        return []





if __name__ == "__main__":
    torrents = get_torrents()

    # debug printout
    for i in torrents:
        print('%r %s: %s %s %.2f%%' % (i.id, i.hashString, i.name, i.status, i.progress))

