import transmissionrpc
import sys

"""
    function: get_torrent
    Returns info on a single torrent

    See: https://github.com/0vercl0k/stuffz/blob/master/transmissionrpc.py
"""
def get_torrent(torrent_to_get):
    client = transmissionrpc.Client('127.0.0.1', port=9091, user='admin', password='admin')
    torrent = client.get_torrent(torrent_to_get)
    return torrent




if __name__ == "__main__":
    torrent = get_torrent(sys.argv[1])
    print('%s: %s %s %.2f%%' % (torrent.hashString, torrent.name, torrent.status, torrent.progress))


