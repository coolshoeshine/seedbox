import transmissionrpc

"""
    function: get_torrent
    Returns info on a single torrent

    See: https://github.com/0vercl0k/stuffz/blob/master/transmissionrpc.py
"""
def start_torrent(torrent_to_start):
    client = transmissionrpc.Client('127.0.0.1', port=9091, user='admin', password='admin')
    torrent = client.start_torrent(torrent_to_start)