import sys
import transmissionrpc
import os

from transmissionrpc import torrent


"""
    function: add_torrent
    Add the specified torrent url to the torrent client
"""
def add_torrent(torrent_file):
    print("here in add_torrent, we received torrent_file: ", torrent_file)

    # client = transmissionrpc.Client(
    #     '127.0.0.1', 
    #     port=9091, 
    #     user='admin', 
    #     password='admin',
    # )
    # torrent = client.add_torrent(torrent_file, download_dir="/home/paul/seedbox/")
    # return torrent


    string_for_transmission = 'transmission-remote --auth admin:admin -a %s.torrent' % (
        torrent_file,
    )
    print("\nstring_for_transmission:\n%s\n\n" % string_for_transmission)

    os.system(string_for_transmission)



if __name__ == "__main__":
    add_torrent(sys.argv[1])

