import sys
import transmissionrpc


"""
    function: remove_torrent
    Remove the specified torrent from the torrent client
"""
def remove_torrent(torrent_to_remove):
    client = transmissionrpc.Client('127.0.0.1', port=9091, user='admin', password='admin')
    client.remove(str(torrent_to_remove))



if __name__ == "__main__":
    remove_torrent(sys.argv[1])
