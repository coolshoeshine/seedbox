if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    sys.path.append("/home/paul/seedbox/server") # necessary for cron job
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import src.dependencies.websocket_pb2 as webrpc
import src.dependencies.websocket_pb2_grpc as webstub
import grpc
import src.bin.node_info as nodes

def testWebsocket():
    print("=== running testWebsocket ===")

    # initialize websocket
    channel_imp = grpc.insecure_channel(nodes.nucCharlie_local['conn_info'])
    stub_msg = msgstub.MessagingStub(channel_imp)
    stub_web = webstub.WebsocketStub(channel_imp)
    stub_ln = lnstub.LightningStub(channel_imp)

    # listen on websocket
    request = webrpc.SubscribeRequest()

    for response in stub_web.Subscribe(request):
        print("incoming message from webscocket:\n%s\n" % response)


if __name__ == "__main__":
    testWebsocket()
