if __name__ == "__main__":
    import sys, os, django
    sys.path.append(os.path.join('.', 'server'))
    sys.path.append("/home/paul/seedbox/server") # necessary for cron job
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "seedbox.settings")
    django.setup()
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import json, grpc
import src.bin.node_info as nodes
from apps.websocket.models import MethodOptions

def testMessage():
    print("=== running testMessage ===")

    # construct the message
    # send_dict = {
    #     'offer_accepted': True,
    #     'contract_id': 'some_contract_id',
    # }
    # msg = json.dumps(send_dict)

    # msg = "torrent_request"
    msg_dict = {
        "method": MethodOptions.TorrentRequest,
    }
    msg = json.dumps(msg_dict)

    print("sending message:\n%s\n" % msg)

    # first interaction with impervious API
    send_message = msgrpc.SendMessageRequest(
        msg=msg,
        pubkey=nodes.nucBob_local['pubkey'],
    )
    print("result of SendMessageRequest:\n%s\n" % send_message)

    # initialize websocket
    channel_imp = grpc.insecure_channel(nodes.nucBob_local['conn_info'])
    stub_msg = msgstub.MessagingStub(channel_imp)
    print("stub_msg:\n%s\n" % stub_msg)

    # send the message
    imp_response = stub_msg.SendMessage(send_message)
    print("imp_response:\n%s\n" % imp_response)


if __name__ == "__main__":
    testMessage()
