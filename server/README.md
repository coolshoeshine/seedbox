# test-api

Django server for Impervious hackathon seedbox

## Activate the virtual environment in VS Code

### clone

> git clone https://gitlab.com/coolshoeshine/seedbox seedbox

### First, create the venv

> python -m venv ./env/

### then activate

> Set-ExecutionPolicy Unrestricted -Scope Process  
> .\server\env\Scripts\activate  

### then, install all the requirements

pip install -r ./server/requirements.txt

## Launch django server (from root directory)

python ./server/manage.py runserver
