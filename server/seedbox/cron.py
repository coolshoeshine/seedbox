import src.dependencies.lightning_pb2 as lnrpc
import src.dependencies.lightning_pb2_grpc as lnstub
import src.dependencies.messaging_pb2 as msgrpc
import src.dependencies.messaging_pb2_grpc as msgstub
import grpc
from apps.content.models import Contract, ContractStatus, ContractInvoices
from apps.seeder.models import Seeder
import src.bin.node_info as nodes
import transmissionrpc
from src.tsm_rpc.get_torrent import get_torrent
from src.tsm_rpc.start_torrent import start_torrent
from src.tsm_rpc.stop_torrent import stop_torrent
import json
import datetime



def log_print(method_name, msg):
    msg_to_print = "[%s %s] %s" % (
        datetime.datetime.now(),
        method_name,
        msg,
    )
    print(msg_to_print)



def seed_upload():
    log_print('seed_upload', 'starting')
    #to be populated with BitTorrent client checker stuff
    #then update Seeder table with new info only if associated contract status is active
    #if contract isn't active, seeding is stopped via BitTorrent RPC
    # Would be nice is client pulled info inputted by the user from the GUI into a db table that we read from
    contracts = Contract.objects.using('localuser').all()
    for contract in contracts:
        seeders =Seeder.objects.using('localuser').all().filter(
            contract=contract
        )
        for seeder in seeders:
            torrent_file = get_torrent(seeder.torrent_id)
            if torrent_file.status == 6: #if it's currently seeding
                if contract.status != ContractStatus.ACTIVE: #and the contract isn't active
                    stop_torrent(seeder.torrent_id)
                else: #the contract is active
                    total_bytes = torrent_file.uploadedEver
                    if  total_bytes > seeder.bytes_seeded:
                        seeder.bytes_delta = total_bytes - seeder.bytes_seeded
                        seeder.bytes_seeded = total_bytes
                        seeder.save()
            elif contract.status == ContractStatus.ACTIVE: #it's not seeding but the contract is active
                start_torrent(seeder.torrent_id)

def generate_and_send_invoice(stub_ln, stub_msg, contract, update_attemps=False):
    log_print('generate_and_send_invoice', 'starting')
    seederObjs = Seeder.objects.using('localuser').all().filter(
            contract=contract
        )
    amt = 0
    for seeder in seederObjs:
        amt += seeder.bytes_delta*seeder.cost_per_byte
    dateTimeObj = datetime.now()
    inv_req = lnrpc.GenerateInvoiceRequest(
        amount=amt,
        memo=f'Seeding invoice {dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")}'
    )
    inv = stub_ln.GenerateInvoice(inv_req)
    inv_msg = {
        "invoice": {
            "contract_id": contract.contract_id,
            "ln_req": inv.invoice,
            "amount": amt
        }
    }
    log_print(
        'generate_and_send_invoice', 
        'inv_msg: %s' % inv_msg,
    )
    msg = json.dumps(inv_msg)
    req = msgrpc.SendMessageRequest(
        msg=msg,
        pubkey=contract.purchaser_pubkey,
    )
    msg_resp = stub_msg.SendMessage(req)
    if not update_attemps:
        ContractInvoices.objects.using('localuser').create(
            contract=contract,
            ln_req=inv.invoice,
            amount=amt
        )
    else:
        cont_inv = ContractInvoices.objects.using('localuser').get(
            contract=contract
        )
        cont_inv.ln_req = inv.invoice
        cont_inv.attempt = 1
        cont_inv.amount = amt
        cont_inv.save()

def invoicing():
    log_print('invoicing', 'starting')
    channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
    stub_ln = lnstub.LightningStub(channel_imp)
    stub_msg = msgstub.MessagingStub(channel_imp)
    
    contracts = Contract.objects.using('localuser').all().filter(
        status=ContractStatus.ACTIVE
    )
    for contract in contracts:
        generate_and_send_invoice(stub_ln, stub_msg, contract)
        

def check_invoice_status():
    log_print('check_invoice_status', 'starting')
    channel_imp = grpc.insecure_channel(nodes.LocalNode['conn_info'])
    stub_ln = lnstub.LightningStub(channel_imp)
    stub_msg = msgstub.MessagingStub(channel_imp)

    invoices = ContractInvoices.objects.using('localuser').all()
    for invoice in invoices:
        check_inv_req = lnrpc.CheckInvoiceRequest(
            invoice=invoice.ln_req
        )
        check_inv_resp = stub_ln.CheckInvoice(check_inv_req)
        if check_inv_resp.paid == True:
            invoice.delete()
        else:
            contract = Contract.objects.using('localuser').get(
                contract=invoice.contract
            )
            if invoice.attempt < 1:
                generate_and_send_invoice(stub_ln, stub_msg, contract, update_attemps=True)
            else:
                contract.status = ContractStatus.CANCELLED
                contract.save()

