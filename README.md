# seedbox

### Installing grpc dependencies
1. Install dependencies 
`$ pip install grpcio grpcio-tools googleapis-common-protos`

2. Clone google's api repository 
`$ git clone https://github.com/googleapis/googleapis.git`

3. Clone grpc-gateway repository 
`$ git clone https://github.com/grpc-ecosystem/grpc-gateway.git`

4. Copy the imp proto you're looking to compile (in this case messaging. proto)
`$ curl -o messaging.proto -s https://raw.githubusercontent.com/imperviousai/imp-releases/main/proto/messaging.proto`

5. Compile the proto file. If anyone knows how to include parent dir for `proto_path` instead of using the unrecommended, two paths method, please let me know
`$ python -m grpc_tools.protoc --proto_path=googleapis:. --proto_path=grpc-gateway:. --python_out=. --grpc_python_out=. messaging.proto`

6. Also compile `google/api/annotations.proto`, `google/api/http.proto`, `protoc_gen_openapiv2/options/annotations.proto` and `protoc_gen_openapiv2/options/openapiv2.proto`
since `messaging_pb2.py` references the `_pb2.py` files from the afformentioned proto files.
```
$ python -m grpc_tools.protoc --proto_path=googleapis:. --python_out=. --grpc_python_out=. googleapis/google/api/annotations.proto
$ python -m grpc_tools.protoc --proto_path=googleapis:. --python_out=. --grpc_python_out=. googleapis/google/api/http.proto
$ python -m grpc_tools.protoc --proto_path=grpc-gateway:. --python_out=. --grpc_python_out=. grpc-gateway/protoc-gen-openapiv2/options/annotations.proto
$ python -m grpc_tools.protoc --proto_path=grpc-gateway:. --python_out=. --grpc_python_out=. grpc-gateway/protoc-gen-openapiv2/options/openapiv2.proto
```

### clone

> git clone https://gitlab.com/coolshoeshine/seedbox seedbox

## Launch django server (from root directory)

```
$ python ./server/manage.py crontab add #start django-crontab
$ python ./server/manage.py runserver
```

