# Notes on Impervious API

## Launch:

(I think this only works on Paul's account)

> impervious --config /home/paul/config.yml

## Tested and verified working message sends (formatted for markdown)

#### Send a message from node 1 to node 2 (pubkey is for node 2)

> curl --location --request POST '127.0.0.1:8882/v1/message/send' \  
> --header 'Content-Type: application/json' \  
> --data-raw '{  
>     "msg": "hello",  
>     "pubkey": "02c5fb01ccff281e827885f2d9faea4713e875b0b968a8b0550ab03109c9c1d451"  
> }'  

#### Send a message from node 2 to node 1 (pubkey is for node 2)

> curl --location --request POST '127.0.0.1:8882/v1/message/send' \  
> --header 'Content-Type: application/json' \  
> --data-raw '{  
>     "msg": "hello",  
>     "pubkey": "025e1ef29516bb69c8de0b041c77c54b1eb46ff4ed566daf0d8bfa04aafe11b486"  
> }'  

#### Try airmon-ster (stew) from discord

> curl --location --request POST '127.0.0.1:8882/v1/message/send' \  
> --header 'Content-Type: application/json' \  
> --data-raw '{  
>     "msg": "hello airmon-ster (stew) from coolshoeshine",  
>     "pubkey": "034c39a618a0e65598eda2633479e718c94a36b9a797170dca27e8d5a39dfdf118"  
> }'  

#### Try Ridgeback from discord

> curl --location --request POST '127.0.0.1:8882/v1/message/send' \  
> --header 'Content-Type: application/json' \  
> --data-raw '{  
>     "msg": "hello Ridgeback from coolshoeshine",  
>     "pubkey": "0284aeb7562c1982a333cade20f75f364bb113b1c917ff8e5d65dff5a74148f1d1"  
> }'  

## Tested and verified working message sends (raw)

#### Send a message from node 1 to node 2 (pubkey is for node 2)

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "hello",
    "pubkey": "02c5fb01ccff281e827885f2d9faea4713e875b0b968a8b0550ab03109c9c1d451"
}'

#### Send a message from node 2 to node 1 (pubkey is for node 2)

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "hello",
    "pubkey": "025e1ef29516bb69c8de0b041c77c54b1eb46ff4ed566daf0d8bfa04aafe11b486"
}'

#### Try airmon-ster (stew) from discord

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "hello airmon-ster (stew) from coolshoeshine",
    "pubkey": "034c39a618a0e65598eda2633479e718c94a36b9a797170dca27e8d5a39dfdf118"
}'

#### Try Ridgeback from discord

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "hello Ridgeback from coolshoeshine",
    "pubkey": "0284aeb7562c1982a333cade20f75f364bb113b1c917ff8e5d65dff5a74148f1d1"
}'