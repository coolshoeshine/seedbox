# Bitcoin install instructions for testnet during Impervious API hackathon

## Ports

P2P Mainnet: 8333  
P2P Testnet: 18333  
RPC: 8332

## Download

> sudo wget https://bitcoin.org/bin/bitcoin-core-0.20.1/bitcoin-0.20.1-x86_64-linux-gnu.tar.gz  

## Unpack, install

> sudo /bin/tar xzf bitcoin-0.20.1-x86_64-linux-gnu.tar.gz  
> sudo /usr/bin/install -m 0755 -o root -g root -t /usr/local/bin bitcoin-0.20.1/bin/*  
> sudo /bin/rm -rf bitcoin-0.20.1/

## mount external disk

This should return 'data', really just a sanity check

> sudo file -s /dev/nvme1n1  

Create an ext4 file system on the external drive

> sudo mkfs -t ext4 /dev/nvme1n1  

Create a directory named data on the external drive

> sudo mkdir /data  

Change group owner and permissions (user can be 'ozzy' or 'ubuntu')

> sudo chgrp ubuntu /data  
> sudo chmod g+w /data  

Mount the external drive on to the data directory

> sudo mount /dev/nvme1n1 /data  

## find the ID of the external drive

Run one of these to get the UUID of the device

> sudo file -s /dev/nvme1n1  
> sudo ls -al /dev/disk/by-uuid/  

## edit fstab to mount the drive on startup

> sudo nano /etc/fstab

Add a line like this, but replace the UUID

> UUID=f85b8c5a-9718-4a56-a12b-6d90b62f33e1 /data ext4 defaults,nofail 0 2

## Create symlink for directory (allows node to use external volume)

> sudo mkdir /data  
> sudo mkdir /data/bitcoin  
> sudo ln -s /data/bitcoin /root/.bitcoin  

## Make conf file

Open editor

> sudo nano /root/.bitcoin/bitcoin.conf  

and

> sudo nano .bitcoin/bitcoin.conf

Both places

Ctrl+X to exit nano, press y to save

## Launch

open tmux window

> tmux  

And launch the node

> sudo bitcoind -daemon
