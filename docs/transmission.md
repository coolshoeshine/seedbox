# Install instructions for transmissionrpc

Reference:

https://help.ubuntu.com/community/TransmissionHowTo

## Download

> sudo apt install transmission-daemon  

test

> transmission-daemon --version  

## Configure username and password

Stop the daemon

> sudo service transmission-daemon stop  

edit settings

> sudo nano /var/lib/transmission-daemon/info/settings.json  

change username and password to admin/admin

then restart

> sudo service transmission-daemon start  
