# install Lightning

## references

https://github.com/lightningnetwork/lnd/blob/master/docs/INSTALL.md

https://github.com/lightningnetwork/lnd/releases

## Download and install

Download the pre-built binary:

> sudo wget https://github.com/lightningnetwork/lnd/releases/download/v0.13.1-beta/lnd-linux-amd64-v0.13.1-beta.tar.gz  

Unpack, install

> sudo /bin/tar xzf lnd-linux-amd64-v0.13.1-beta.tar.gz  
> sudo /usr/bin/install -m 0755 -o root -g root -t /usr/local/bin lnd-linux-amd64-v0.13.1-beta/lnd  
> sudo /usr/bin/install -m 0755 -o root -g root -t /usr/local/bin lnd-linux-amd64-v0.13.1-beta/lncli  
> sudo /bin/rm -rf lnd-linux-amd64-v0.13.1-beta/  

## copy the conf file

> sudo nano .lnd/lnd.conf  
> sudo nano /root/.lnd/lnd.conf  

## Launch

> sudo lnd --bitcoin.testnet  

## create a wallet

> sudo lncli create  

## get pubkey

> sudo lncli --network="testnet" getinfo

## init connection to testnet faucet

> sudo lncli --network="testnet" connect 0270685ca81a8e4d4d01beec5781f4cc924684072ae52c507f8ebe9daf0caaab7b@159.203.125.125

Connect to Alice:

> sudo lncli --network="testnet" connect 030e47300c9318c4193af7f64455eb8aced894e75a97fb377cbcd12e1383c13a01@192.168.254.201

Connect to Bob:

> sudo lncli --network="testnet" connect 028ed0c1d3bf3e5d5dabe3f31121fa62361e3b69d129d90edef266f5b62f9fa477@192.168.254.175

Connect to Charlie:

> sudo lncli --network="testnet" connect 02fc8fb20c3c28db153682abff41e97964fb083c963321fdb254da398da2a814d8@192.168.254.176

Connect to Alice_ec2:

> sudo lncli --network="testnet" connect 02aef5fc4c2f7c76265f3d0f63a5d5fc75f1ca6e4fe737860a4e04084aba8df9df@18.144.164.229

Connect to Bob_ec2:

> sudo lncli --network="testnet" connect 03c5a0ee7a5c69cd987edb9d3cbbf7339e515f4624b7d7a12ef9889d3c38cfe0e0@54.215.64.70




## lncli commands

Reference:

https://github.com/tomosaigon/lncli-commands

> sudo lncli --network="testnet" listchannels  
> sudo lncli --network="testnet" listpeers  
> sudo lncli --network="testnet" channelbalance  
> sudo lncli --network="testnet" listinvoices  
> sudo lncli --network="testnet" queryroutes <PUBKEY> <AMOUNT>  
> sudo lncli --network="testnet" getchaninfo <CHAN_ID>  
> sudo lncli --network="testnet" getnetworkinfo  
> sudo lncli --network="testnet" listunspent  
> sudo lncli --network="testnet" feereport  
> sudo lncli --network="testnet" walletbalance  
> sudo lncli --network="testnet" newaddress p2wkh  
> sudo lncli --network="testnet" openchannel --node_key=<ID_PUBKEY> --local_amt=<AMOUNT> --push_amt=<AMOUNT>  


> sudo lncli --network="testnet" openchannel --node_key=02fc8fb20c3c28db153682abff41e97964fb083c963321fdb254da398da2a814d8 --local_amt=20000 --push_amt=10000  

Query route from Charlie to Bob:

> sudo lncli --network="testnet" queryroutes 028ed0c1d3bf3e5d5dabe3f31121fa62361e3b69d129d90edef266f5b62f9fa477 100  

Query route from Bob to Charlie:

> sudo lncli --network="testnet" queryroutes 02fc8fb20c3c28db153682abff41e97964fb083c963321fdb254da398da2a814d8 100  

On ec2, from Alice to Bob:

> sudo lncli --network="testnet" queryroutes 03c5a0ee7a5c69cd987edb9d3cbbf7339e515f4624b7d7a12ef9889d3c38cfe0e0 1  

#### Test payment

on node 1:

> sudo lncli --network="testnet" addinvoice --amt=100 --memo="test payment"  

on node 2:

> sudo lncli --network="testnet" decodepayreq --pay_req=<PAY_REQ>  
> sudo lncli --network="testnet" sendpayment --pay_req=<PAY_REQ>  

or

> sudo lncli --network="testnet" payinvoice --pay_req=<PAY_REQ>  

on node 1:

> sudo lncli --network="testnet" lookupinvoice --rhash=<R_HASH>  

