# Install Impervious AI binaries

## references

https://www.impervious.ai/

https://docs.impervious.ai/

https://faucet.lightning.community/

https://github.com/imperviousai/imp-releases

https://github.com/imperviousai/imp-releases/releases

## download and install

Download the pre-built binary:

> sudo wget https://github.com/imperviousai/imp-releases/releases/download/v0.1.4/impervious-v0.1.4-linux-amd64.tar.gz  

Unpack, install

> sudo /bin/tar xzf impervious-v0.1.4-linux-amd64.tar.gz  
> sudo /usr/bin/install -m 0755 -o root -g root -t /usr/local/bin impervious  
> sudo /bin/rm -rf impervious  

## conf file

> sudo mkdir .imp  
> sudo nano .imp/config.yml  

## Launch

> sudo impervious --config /home/ozzy/.imp/config.yml

or if on an ec2 instance:

> sudo impervious --config /home/ubuntu/.imp/config.yml