# Info on the testnet nodes running bitcoind + LND + imp

## Alice

ip address: 18.144.164.229

ssh -i "C:\Users\oenri\Documents\work\ref\ozzy-MSI-laptop.pem" ubuntu@18.144.164.229

LND pubkey: 02aef5fc4c2f7c76265f3d0f63a5d5fc75f1ca6e4fe737860a4e04084aba8df9df

## Bob

ip address: 54.215.64.70

ssh -i "C:\Users\oenri\Documents\work\ref\ozzy-MSI-laptop.pem" ubuntu@54.215.64.70

LND pubkey: 03c5a0ee7a5c69cd987edb9d3cbbf7339e515f4624b7d7a12ef9889d3c38cfe0e0

## Test messages

#### To Alice:

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "0x74657374206d6573736167650a",
    "pubkey": "02aef5fc4c2f7c76265f3d0f63a5d5fc75f1ca6e4fe737860a4e04084aba8df9df"
}'

#### To Bob:

curl --location --request POST '127.0.0.1:8882/v1/message/send' \
--header 'Content-Type: application/json' \
--data-raw '{
    "msg": "0x74657374206d6573736167650a",
    "pubkey": "03c5a0ee7a5c69cd987edb9d3cbbf7339e515f4624b7d7a12ef9889d3c38cfe0e0"
}'



